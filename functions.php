<?php

// Enqueue child theme style.css
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

}
function twentysixteen_child_setup() {
    add_theme_support( 'post-formats', array(
       'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'audio',
    ) );
}
add_action( 'after_setup_theme', 'twentysixteen_child_setup', 11 );

//hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'create_video_category_hierarchical_taxonomy', 0 );
 
//create a custom taxonomy name it topics for your posts
 
function create_video_category_hierarchical_taxonomy() {
 
// Add new taxonomy, make it hierarchical like categories
//first do the translations part for GUI
 
  $labels = array(
    'name' => _x( 'Video Categories', 'taxonomy general name' ),
    'singular_name' => _x( 'Video Category', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Video Categories' ),
    'all_items' => __( 'All Video Categories' ),
    'parent_item' => __( 'Parent Video Category' ),
    'parent_item_colon' => __( 'Parent Topic:' ),
    'edit_item' => __( 'Edit Video Category' ), 
    'update_item' => __( 'Update Video Category' ),
    'add_new_item' => __( 'Add New Video Category' ),
    'new_item_name' => __( 'New Video Category Name' ),
    'menu_name' => __( 'Video Categories' ),
  );    
 
// Now register the taxonomy
 
  register_taxonomy(
  'video_categories',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces). 
        'videos',
  array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'videos' ),
  ));
 
}

//Registering Video Post Type 
add_action( 'init', 'register_videopost', 20 );



function register_videopost() {
    $labels = array(
        'name' => _x( 'Video', 'my_custom_post','custom' ),
        'singular_name' => _x( 'Video', 'my_custom_post', 'custom' ),
        'add_new' => _x( 'Add New', 'my_custom_post', 'custom' ),
        'add_new_item' => _x( 'Add New VideoPost', 'my_custom_post', 'custom' ),
        'edit_item' => _x( 'Edit VideoPost', 'my_custom_post', 'custom' ),
        'new_item' => _x( 'New VideoPost', 'my_custom_post', 'custom' ),
        'view_item' => _x( 'View VideoPost', 'my_custom_post', 'custom' ),
        'search_items' => _x( 'Search VideoPosts', 'my_custom_post', 'custom' ),
        'not_found' => _x( 'No VideoPosts found', 'my_custom_post', 'custom' ),
        'not_found_in_trash' => _x( 'No VideoPosts found in Trash', 'my_custom_post', 'custom' ),
        'parent_item_colon' => _x( 'Parent VideoPost:', 'my_custom_post', 'custom' ),
        'menu_name' => _x( 'Video Type Posts', 'my_custom_post', 'custom' ),
    );

$args = array(
        'labels' => $labels,
        'hierarchical' => false,
        'description' => 'Video Type Posts',
        'supports' => array( 'title', 'editor', 'revisions', 'post-formats' ),
        'taxonomies' => array( 'post_tag','video_categories'),
	       'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'menu_icon' => get_stylesheet_directory_uri() . '/functions/panel/images/catchinternet-small.png',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => array('slug' => 'videos/%video_categories%','with_front' => FALSE),
        'public' => true,
        'has_archive' => 'videos',
        'capability_type' => 'post'
    );  
    register_post_type( 'videos', $args );//max 20 charachter cannot contain capital letters and spaces
} 


function allowed_video_formats() {

    return array('video' );
}

function post_format_support_filter() {

    $screen = get_current_screen();

    // Bail if not on the projects screen.
    if ( empty( $screen->post_type ) ||  $screen->post_type !== 'videos' )
        return;

    // Check if the current theme supports formats.
    if ( current_theme_supports( 'post-formats' ) ) {

        $formats = get_theme_support( 'post-formats' );

        // If we have formats, add theme support for only the allowed formats.
        if ( isset( $formats[0] ) ) {
            $new_formats = array_intersect( $formats[0], allowed_video_formats() );

            // Remove post formats support.
            remove_theme_support( 'post-formats' );

            // If the theme supports the allowed formats, add support for them.
            if ( $new_formats )
                add_theme_support( 'post-formats', $new_formats );
        }
    }
	    // Filter the default post format.
    add_filter( 'option_default_post_format', 'default_post_format_filter', 95 );

}

add_action( 'load-post.php',     'post_format_support_filter' );
add_action( 'load-post-new.php', 'post_format_support_filter' );
add_action( 'load-edit.php',     'post_format_support_filter' );

function default_post_format_filter( $format ) {
	
	

    return in_array( $format, allowed_video_formats() ) ? $format : 'video';
}


function filter_post_type_link($link, $post)
{
    if ($post->post_type != 'videos')
        return $link;

    if ($cats = get_the_terms($post->ID, 'video_categories'))
        $link = str_replace('%video_categories%', array_pop($cats)->slug, $link);
    return $link;
}
add_filter('post_type_link', 'filter_post_type_link', 10, 2); 


// create shortcode to list all clothes which come in blue
add_shortcode( 'video-list', 'video_listings' );
function video_listings( $atts ) {
    ob_start();
	
	?>
	<h4>Фильтр по Видео категориям</h4>
<form action="" method="GET" id="videos">
<select name="vidcats" id="vidcats" onchange="return MyForm(this);" >
<option value="" <?php echo ($_GET['vidcats'] == '') ? ' selected="selected"' : ''; ?>>Показать Все</option>
<?php 
    $categories = get_categories('taxonomy=video_categories&post_type=videos'); 
    foreach ($categories as $category) : 
	

    echo '<option value="'.$category->term_id.'"';
    echo ($_GET['vidcats'] == ''.$category->name.'') ? ' selected="selected"' : '';
    echo '>'.$category->name.'</option>';
    endforeach; 
?>
</select>
</form>

<script type="text/javascript">
function MyForm(selectObject)
{
   var value = selectObject.value;
   if (value !='') {
jQuery('.type-videos').hide();   
jQuery('.cat_'+value).show();

   }else{
	 jQuery('.type-videos').show();     
	   
   }
   

  return true;
}
</script>

<?php
    $query = new WP_Query( array(
        'post_type' => 'videos',
        'posts_per_page' => -1,
        'order' => 'ASC',
        'orderby' => 'title',
    ) );
    if ( $query->have_posts() ) { ?>
        <ul class="video-listing">
            <?php while ( $query->have_posts() ) : $query->the_post(); ?>
			
			<?php $post_categories = wp_get_post_categories( get_the_ID()); 
			$category = get_the_terms( $post->ID, 'video_categories' );
			$cls = null;

		foreach ($category as $ct) {
				$cls .= ' cat_'.$ct->term_id;
				
			}

			?>
            <li id="post-<?php the_ID(); ?>" <?php post_class($cls); ?>>
                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			<?php 	echo the_content(); ?>
            </li>
            <?php endwhile;
            wp_reset_postdata(); ?>
        </ul>
    <?php $myvariable = ob_get_clean();
    return $myvariable;
    }
}
